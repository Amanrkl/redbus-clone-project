import { SIGN_IN_USER, UPDATE_SEARCH_PARAMS } from "../actions/users";

import { v4 as uuidv4 } from 'uuid';

const initialState = {
    User: {
        isLogin: false,
        id: '',
        name: '',
        phoneNo: '',
    },

    searchParameters: {
        source: '',
        destination: '',
        date: '',
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SIGN_IN_USER: {
            const newState = {
                ...state,
                User: {
                    ...state.User,
                    id: uuidv4(),
                    ...action.payload,

                }
            };

            return newState;

        }
        case UPDATE_SEARCH_PARAMS: {
            const newState = {
                ...state,
                searchParameters: {
                    ...action.payload,
                }
            };

            return newState;

        }
        default:
            return state;
    }
}

