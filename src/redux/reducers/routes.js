import { SORT_ROUTE, FILTER_ROUTE } from "../actions/routes";

const initialState = {
    RouteList: [{
        id: 1,
        source_bus_stand: 'Anand Rao Circle',
        destination_bus_stand: 'Lakdikapul',
        start_date: '11-02-2023',
        end_date: '12-02-2023',
        start_time: '22:00',
        end_time: '7:30',
        bus_details: {
            id: 2,
            name: 'intercity travels',
            type: 'Volvo Multi Axle Sleeper',
            rating: {
                rate: 3.8,
                count: 652,
            },
            total_seats: 45,
            available_seats: 22,
            single_seats: 8,
            prices: ['2099', '2499', '2399', '2999'],
            amentities: ['wifi', 'blankets'],

        },
    },
    {
        id: 2,
        source_bus_stand: 'Anand Rao Circle',
        destination_bus_stand: 'Lakdikapul',
        start_date: '11-02-2023',
        end_date: '12-02-2023',
        start_time: '22:15',
        end_time: '06:45',
        bus_details: {
            id: 5,
            name: 'SRS travels',
            type: 'Bharat Benz AC Sleeper ',
            rating: {
                rate: 4.6,
                count: 391,
            },
            total_seats: 50,
            available_seats: 36,
            single_seats: 12,
            prices: ['1800', '2000', '2100'],
            amentities: ['wifi', 'blankets', 'charging point'],

        },
    }, {
        id: 3,
        source_bus_stand: 'Hebbal',
        destination_bus_stand: 'Lakdikapul',
        start_date: '11-02-2023',
        end_date: '12-02-2023',
        start_time: '18:15',
        end_time: '06:35',
        bus_details: {
            id: 1,
            name: 'ksm road lines',
            type: 'AC Sleeper',
            rating: {
                rate: 4.7,
                count: 946,
            },
            total_seats: 50,
            available_seats: 33,
            single_seats: 10,
            prices: ['2899', '2999', '3199', '3299'],
            amentities: ['wifi', 'blankets'],

        },
    }, {
        id: 4,
        source_bus_stand: 'Anand Rao Circle',
        destination_bus_stand: 'Lakdikapul',
        start_date: '11-02-2023',
        end_date: '12-02-2023',
        start_time: '16:15',
        end_time: '08:35',
        bus_details: {
            id: 3,
            name: 'zingbus',
            type: 'Seater',
            rating: {
                rate: 4.6,
                count: 1114,
            },
            total_seats: 50,
            available_seats: 26,
            single_seats: 9,
            prices: ['2199', '2499', '2999', '3099'],
            amentities: ['wifi', 'blankets'],

        },
    }, {
        id: 5,
        source_bus_stand: 'Hebbal',
        destination_bus_stand: 'Lakdikapul',
        start_date: '11-02-2023',
        end_date: '12-02-2023',
        start_time: '16:30',
        end_time: '09:30',
        bus_details: {
            id: 4,
            name: 'VRL travels',
            type: 'Sleeper',
            rating: {
                rate: 4.2,
                count: 2444,
            },
            total_seats: 50,
            available_seats: 28,
            single_seats: 8,
            prices: ['1900', '2000', '2300'],
            amentities: ['wifi', 'blankets'],

        },
    }],
    FilterCategories: {
        "dt6 am to 12 pm": false,
        "dt12 pm to 6 pm": false,
        "dtBefore 6 am": false,
        "dtAfter 6 pm": false,

        "bt_SEATER": false,
        "bt_SLEEPER": false,
        "bt_AC": false,
        "bt_NONAC": false,

        "atBefore 6 am": false,
        "at6 am to 12 pm": false,
        "at12 pm to 6 pm": false,
        "atAfter 6 pm": false,
    },
    SortCategories: {
        departure: false,
        duration: false,
        arrival: false,
        rating: false,
        fare: false,
        seatsAvailability: false
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SORT_ROUTE: {
            const newState = {
                ...state,
                SortCategories: {
                    ...initialState.SortCategories,
                    ...action.payload,
                }
            };

            return newState;

        }
        case FILTER_ROUTE: {
            console.log(action.payload)
            const newState = {
                ...state,
                FilterCategories: {
                    ...state.FilterCategories,
                    ...action.payload,
                }
            };

            return newState;

        }
        default:
            return state;
    }
}