import { UPDATE_SELECTED_SEAT, RESET_BUS_STATE } from "../actions/bus";
const initialState = {
    BusLayout: {
        LowerDeck: {
            first_row: [
                {
                    seat_no: "L1",
                    seat_type: "sleeper female",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2599,
                },
                {
                    seat_no: "L2",
                    seat_type: "sleeper female",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2599,
                },
                {
                    seat_no: "L3",
                    seat_type: "sleeper female",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L4",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L5",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L6",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L7",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ],
            second_row: [
                {
                    seat_no: "L8",
                    seat_type: "sleeper female",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2599,
                },
                {
                    seat_no: "L9",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2599,
                },
                {
                    seat_no: "L10",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L11",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L12",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L13",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L14",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ],
            third_row: [
                {
                    seat_no: "L15",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2399,
                },
                {
                    seat_no: "L16",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2399,
                },
                {
                    seat_no: "L17",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L18",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L19",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L20",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "L21",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ]
        },
        UpperDeck: {
            first_row: [
                {
                    seat_no: "U1",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U2",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U3",
                    seat_type: "sleeper female",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U4",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U5",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U6",
                    seat_type: "sleeper female",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U7",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ],
            second_row: [
                {
                    seat_no: "U8",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U9",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U10",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U11",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U12",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U13",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U14",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ],
            third_row: [
                {
                    seat_no: "U15",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U16",
                    seat_type: "sleeper",
                    seat_availability: false,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U17",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U18",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U19",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U20",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                },
                {
                    seat_no: "U21",
                    seat_type: "sleeper",
                    seat_availability: true,
                    seat_select: false,
                    seat_price: 2099,
                }
            ]
        }
    },

    SeatsSelected: [],

    SelectedBusId: null,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case UPDATE_SELECTED_SEAT: {
            let filteredSeatsSelected = [];
            let busId = null;
            const { id, Selectedseat } = action.payload;
            if (!Selectedseat.seat_select) {
                filteredSeatsSelected = state.SeatsSelected.filter((seat) => {
                    console.log("reduce", Selectedseat)
                    return (seat.seat_no !== Selectedseat.seat_no)
                })
            } else {
                filteredSeatsSelected = state.SeatsSelected;
                filteredSeatsSelected.push(Selectedseat)
            }
            if (filteredSeatsSelected.length > 0) {
                busId = id;
            }

            const newState = {
                ...state,
                SeatsSelected: [
                    ...filteredSeatsSelected,
                ],
                SelectedBusId: busId,
            };

            console.log("state updated", newState.SelectedBusId)
            return newState;

        }
        case RESET_BUS_STATE: {
            const newState = {
                ...initialState,
                SeatsSelected: [],
            }

            return newState;
        }
        default:
            return state;
    }
}