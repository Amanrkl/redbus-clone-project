import { combineReducers } from "redux";

import users from "./users";
import routes from "./routes";
import bus from "./bus";

export default combineReducers({
    users,
    routes,
    bus,
});
