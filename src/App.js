import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import './App.css';
import SignupForm from './components/SignupForm';
import Header from './components/Header';
import Home from "./components/HomePage/Home";
import BusTickets from './components/BusTickets';
import PaymentDetails from "./components/PaymentDetails";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

function App() {
  return (
    <Router>
      <div className="App">
        <>
          <Route exact path={["/", "/bus-tickets"]}>
            <Header />
            <SignupForm />
          </Route>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/bus-tickets">
              <BusTickets />
            </Route>
            <Route exact path="/pay/PaymentDetails">
              <PaymentDetails />
            </Route>
          </Switch>
        </>
      </div>

    </Router>
  );
}

export default App;

