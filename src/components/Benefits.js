import "../style/Benefits.css";

function Benefits() {
    return (
        <div className="container py-4 d-flex align-items-center flex-column">
            <div className="dib">
                <img
                    src="//s1.rdbuz.com/web/images/home/promise.png"
                    height="100"
                />
            </div>
            <div className="head-main">We promise to deliver</div>

            <div className="benefits-box row">
                <div className="benefit col">
                    <div className="imgCont">
                        <img
                            src="//s2.rdbuz.com/web/images/home/benefits.png"
                            height="90"
                            alt=''
                        />
                    </div>
                    <div className="title">UNMATCHED BENEFITS</div>
                    <div className="second-level-heading">
                        We take care of your travel beyond ticketing by
                        providing you with innovative and unique benefits.
                    </div>
                </div>
                <div className="benefit col">
                    <div className="imgCont">
                        <img
                            src="//s1.rdbuz.com/web/images/home/customer_care.png"
                            height="90"
                            alt=''
                        />
                    </div>
                    <div className="title">SUPERIOR CUSTOMER SERVICE</div>
                    <div className="second-level-heading descCont">
                        We put our experience and relationships to good use and
                        are available to solve your travel issues.
                    </div>
                </div>
                <div className="benefit col">
                    <div className="imgCont">
                        <img
                            src="//s1.rdbuz.com/web/images/home/lowest_Fare.png"
                            height="90"
                            alt=''
                        />
                    </div>
                    <div className="title">LOWEST PRICES</div>
                    <div className="second-level-heading descCont">
                        We always give you the lowest price with the best
                        partner offers.
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Benefits;
