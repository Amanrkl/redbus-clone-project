
import '../style/Offers.css';

const Offers = () => {
    return (
        <div className="offers container d-flex justify-content-center flex-wrap">
            <div className="offer m-2 p-3">
                <p className="offer-det">Save upto Rs 300 on Ap and TS route</p>
                <div className="offer-img">
                    <img src="https://st.redbus.in/images/offers/superhit_rav/1_1.png" />
                </div>
                <p className="offer-code">
                    Use Code SUPERHIT
                </p>

            </div>
            <div className="offer m-2 p-3">

                <p className="offer-det">
                    Save upto Rs 200 on Delhi and North routes
                </p>
                <div className="offer-img">
                    <img src="https://st.redbus.in/Images/INDOFFER/RB200/274x148.png" />
                </div>
                <p className="offer-code">
                    Use code RB200
                </p>
            </div>
        </div>
    );
};

export default Offers;
