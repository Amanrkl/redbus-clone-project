import React, { Component } from "react";

import "../style/BusTicket.css";
import Deck from "./Deck";

import { connect } from "react-redux";
import { RESET_BUS_STATE } from "../redux/actions/bus";
import TicketCart from "./TicketCart";

class BusTicket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isViewSeat: false,
        };
    }

    handleViewClick = () => {
        let isViewSeat = !this.state.isViewSeat;

        this.setState({
            isViewSeat,
        });
        console.log(this.props.SeatsSelected);
        if (this.props.SeatsSelected) {
            this.props.resetBusState();
        }
    };
    render() {
        const { busRoute, busLayout, SeatsSelected } = this.props;
        const busDetails = busRoute.bus_details;

        return (
            <>
                <div className="mx-2 container-fluid">
                    <div className="bus-item p-2 mt-4">
                        <div className=" bus-item-details p-2 d-flex flex-column">
                            <div className="row d-flex w-100">
                                <div className="col-3">
                                    <p className="bus-name text-capitalize mb-1">
                                        {busDetails.name}
                                    </p>
                                    <p className="bus-type">
                                        {busDetails.type}
                                    </p>
                                </div>
                                <div className="col-1">
                                    <p className="dp-time mb-1">
                                        {busRoute.start_time}
                                    </p>
                                    <p className="dp-location">
                                        {busRoute.source_bus_stand}
                                    </p>
                                </div>
                                <div className="col-1">
                                    <div className="duration">09h 30m</div>
                                </div>
                                <div className="col-1">
                                    <div className="bp-time mb-1">
                                        {busRoute.end_time}
                                    </div>
                                    <div className="bp-location">
                                        {busRoute.destination_bus_stand}
                                    </div>
                                </div>
                                <div className="col-1">
                                    <div className="rating-sec">
                                        <div className="rating badge mb-1">
                                            <span className="rate">
                                                <i className="fa-regular fa-star"></i>{" "}
                                                {busDetails.rating.rate}
                                            </span>
                                        </div>
                                    </div>
                                    <div className="rating-count">
                                        <span className="">
                                            <i className="fa-solid fa-users"></i>{" "}
                                            {busDetails.rating.count}
                                        </span>
                                    </div>
                                </div>
                                <div className="col-2 text-center">
                                    <div className="seat-fare ">
                                        <div className="starts">
                                            Starts from
                                        </div>
                                        <div className="fare">
                                            INR{" "}
                                            <span>{busDetails.prices[0]}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-3 text-center">
                                    <div className="seat-left">
                                        {busDetails.available_seats}
                                        <span className="l-color">
                                            {" "}
                                            Seats available
                                        </span>
                                    </div>
                                    <div className="single-left ">
                                        {busDetails.single_seats}
                                        <span className="l-color"> Single</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex justify-content-end">
                            <button
                                className="view-seats btn btn-danger"
                                onClick={this.handleViewClick}
                            >
                                {this.state.isViewSeat ? "Hide" : "View"} Seats
                            </button>
                        </div>
                    </div>
                    {this.state.isViewSeat && (
                        <div className="seat-container-div p-3">
                            <div className="seat-container">
                                <div className="price-ploy d-flex justify-content-between">
                                    <div className="row w-75">
                                        {/* <p className="seat-label col-4">
                                        Seat Price
                                    </p> 
                                    <div className="price-ploy-wrapper col">
                                        <ul className="multiFare">
                                            <li
                                                data-price="2899"
                                                className="mulfare"
                                            >
                                                2899
                                            </li>
                                        </ul>
                                    </div> */}
                                    </div>
                                    <div
                                        className="seat-close"
                                        onClick={this.handleViewClick}
                                    >
                                        <i className="fa-solid fa-xmark"></i>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="leftContent col-6">
                                        <div className="seat-layout">
                                            <div className="lower-canvas">
                                                <div className="label">
                                                    Lower Deck
                                                </div>
                                                <Deck
                                                    deckLayer={
                                                        busLayout.LowerDeck
                                                    }
                                                    busId={busRoute.id}
                                                />
                                            </div>
                                            <div className="upper-canvas">
                                                <div className="label">
                                                    Upper Deck
                                                </div>
                                                <Deck
                                                    deckLayer={
                                                        busLayout.UpperDeck
                                                    }
                                                    busId={busRoute.id}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="rightContent col-6">
                                        {SeatsSelected.length === 0 ? (
                                            <div className="legend container w-50">
                                                <h3 className="legend-header">
                                                    seat legend
                                                </h3>

                                                <div className="legend-text row flex-wrap">
                                                    <div className="seat-legend col">
                                                        <div className="legend-label">
                                                            available
                                                        </div>
                                                        <div className="available-sleep"></div>
                                                    </div>
                                                    <div className="seat-legend col">
                                                        <div className="legend-label">
                                                            unavailable
                                                        </div>
                                                        <div className="unavailable-sleep"></div>
                                                    </div>
                                                    <div className="seat-legend col">
                                                        <div className="legend-label">
                                                            female
                                                        </div>
                                                        <div className="female-sleep"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : (
                                            <TicketCart
                                                busRoute={busRoute}
                                                SeatsSelected={SeatsSelected}
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </>
        );
    }
}

const mapStateToProps = (stateInStore) => {
    return {
        SeatsSelected: stateInStore.bus.SeatsSelected,
    };
};

const mapDispatchToProps = {
    resetBusState: (payload) => {
        return {
            type: RESET_BUS_STATE,
            payload,
        };
    },
};
export default connect(mapStateToProps, mapDispatchToProps)(BusTicket);
