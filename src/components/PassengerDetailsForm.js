import React, { Component } from "react";

import { Link } from "react-router-dom";
import "../style/PassengerDetailsForm.css";

class PassengerDetailsForm extends Component {
    render() {
        const { seatDetails, totalAmount } = this.props;

        return (
            <div
                className="offcanvas offcanvas-end container"
                tabIndex="-1"
                id="offcanvasExample"
                aria-labelledby="offcanvasExampleLabel"
            >
                <div className="offcanvas-header justify-content-start">
                    <button
                        type="button"
                        className="btn-close text-reset"
                        data-bs-dismiss="offcanvas"
                        aria-label="Close"
                    ></button>
                    <h5
                        className="offcanvas-title text-center w-75"
                        id="offcanvasExampleLabel"
                    >
                        Passenger Details
                    </h5>
                </div>
                <div className="offcanvas-body">
                    <div className="passengers">
                        <h6 className="passenger-info-header pb-2">
                            <span className="user-icon">
                                <i
                                    className="fa fa-user-circle-o"
                                    aria-hidden="true"
                                ></i>{" "}
                            </span>
                            <span>Passenger Information</span>
                        </h6>
                        <div className="passengers-info-content">
                            {seatDetails.map((seat, index) => {
                                return (
                                    <div className="passenger-info p-3 mt-2" key={seat}>
                                        <div
                                            className="passenger_sub_title d-flex gap-2"
                                        >
                                            <span className="passenger_priority">
                                                Passenger {index + 1} {" | "}
                                            </span>
                                            <span className="passenger_seat">
                                                Seat {seat}
                                            </span>
                                        </div>
                                        <div className="passenger_contact_container row">
                                            <div className=" col-md-12 mb-3">
                                                <label htmlFor="passenger-name" className="form-label">Name</label>
                                                <input type="text" className="form-control" id="passenger-name" placeholder="Name" />
                                            </div>
                                            <div className='col-md-6 mb-2'>
                                                <label className="form-label">
                                                    Gender
                                                </label>
                                                <div>
                                                    <div className="form-check form-check-inline">
                                                        <input
                                                            className="form-check-input"
                                                            type="radio"
                                                            name={`gender-${seat}`}
                                                            id={`inlineRadio1-${seat}`}
                                                            value="male"
                                                        // onChange={this.handleInputChange}
                                                        />
                                                        <label
                                                            className="form-check-label"
                                                            htmlFor={`inlineRadio1-${seat}`}>
                                                            Male
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input
                                                            className="form-check-input"
                                                            type="radio"
                                                            name={`gender-${seat}`}
                                                            id={`inlineRadio2-${seat}`}
                                                            value="female"
                                                        // onChange={this.handleInputChange}
                                                        />
                                                        <label
                                                            className="form-check-label"
                                                            htmlFor={`inlineRadio2-${seat}`}>
                                                            Female
                                                        </label>
                                                    </div>
                                                </div>
                                                {/* {errors.gender
                                            && <div className="errorMessage">
                                                {errors.gender}
                                            </div>} */}
                                            </div>
                                            <div className='col-md-6 mb-2'>

                                                <label
                                                    htmlFor="age"
                                                    className="form-label">
                                                    Age
                                                </label>

                                                <input
                                                    type="number"
                                                    className="form-control"
                                                    name="age"
                                                    placeholder="Enter your age"
                                                    // onChange={this.handleInputChange}
                                                    id="age"
                                                />
                                                <div className="invalid-feedback">
                                                    {/* {errors.age} */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}

                        </div>
                    </div>
                    <div className="contact">
                        <h6 className="contact-info-header mt-3 pb-2">
                            <span className="user-icon">
                                <i className="fa-regular fa-envelope"></i>{" "}
                            </span>
                            <span>Contact Details</span>
                        </h6>
                        <div className="contact-info-content">
                            <div className="p-3 mt-2" >
                                <div
                                    className="contact_sub_title"
                                >
                                    <p className="contact-message px-2">Your ticket will be sent to these details</p>
                                </div>
                                <div className="contact_container row">
                                    <div className='mb-2'>
                                        <label
                                            htmlFor="email"
                                            className="form-label">
                                            Email
                                        </label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            name="email"
                                            placeholder="Email"
                                            // onChange={this.handleInputChange}
                                            id="email"
                                        />
                                    </div>
                                    <div className='mb-2'>
                                        <label
                                            htmlFor="phoneNo"
                                            className="form-label">
                                            Phone
                                        </label>
                                        <input
                                            type="number"
                                            className="form-control"
                                            name="phoneNo"
                                            placeholder="Phone"
                                            // onChange={this.handleInputChange}
                                            id="phoneNo"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="amount-summary py-2">
                        <p className="fare-terms">By clicking on proceed, I agree that I have read and understood the TnCs and the Privacy Policy</p>
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="fare-summary">
                                <span>Total Amount:{" "}</span>
                                <span>INR {totalAmount}</span>
                            </div>
                            <Link to="/pay/PaymentDetails">
                                <button className="btn btn-danger">
                                    PROCEED TO PAY
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PassengerDetailsForm;
