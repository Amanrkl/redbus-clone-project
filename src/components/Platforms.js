import React, { Component } from "react";

import "../style/Platform.css";

class Platforms extends Component {
    render() {
        return (
            <div className="container d-flex justify-content-between align-items-center py-4">
                <div className="platform-feature mt-4 p-4">
                    <h3 className="text-uppercase">Convenience On-the-go.</h3>

                    <div className="platform-description">
                        <p>
                            Enjoy the following exclusive features on the redBus
                            app
                        </p>
                        <p>
                            Last Minute Booking - In a hurry to book a bus at
                            the last minute? Choose the bus passing from your
                            nearest boarding point and book in a few easy steps.
                        </p>
                        <p>
                            Boarding Point Navigation - Never lose your way
                            while travelling to your boarding point!
                        </p>
                        <p>
                            Comprehensive Ticket Details - Everything that you
                            need to make the travel hassle free - rest stop
                            details, boarding point images, chat with
                            co-passengers, wake-up alarm and much more!
                        </p>
                    </div>

                    <div id="download-sms" className="">
                        <p>
                            Enter your mobile number below to download the
                            redBus mobile app.
                        </p>

                        <div id="phoneWrapper">
                            <div className="phone_number_sec">
                                <input
                                    type="text"
                                    id="smsTXTBOX"
                                    placeholder="Enter your mobile number"
                                    className="LB"
                                />
                            </div>
                            <button className="smsBtn btn btn-danger mt-2">
                                SMS me the link
                            </button>
                        </div>
                    </div>
                    <div className="app_icons mt-2">
                        <a
                            href="https://itunes.apple.com/in/app/redbus/id733712604?mt=8"
                            target="_blank"
                            className="apple icon-iPhone_download"
                        >
                            <img src="//s2.rdbuz.com/web/images/home/sgp/iPhone_download.png" />
                        </a>
                        <a
                            href="https://play.google.com/store/apps/details?id=in.redbus.android&amp;hl=en"
                            target="_blank"
                            className="google icon-Google_download"
                        >
                            <img src="//s3.rdbuz.com/web/images/home/sgp/Google_download.png" />
                        </a>
                    </div>
                </div>
                <div className="phone">
                    <img
                        src="//s1.rdbuz.com/web/images/home/IOS_Android_device.png"
                        className="w-75"
                    />
                </div>
            </div>
        );
    }
}

export default Platforms;
