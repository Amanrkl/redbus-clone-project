import React, { Component } from "react";

import { FILTER_ROUTE } from "../redux/actions/routes";
import { connect } from "react-redux";

class FilterBusTickets extends Component {

    handleFilter = (event) => {
        const name = event.target.name;
        const value = event.target.checked;

        this.props.FilterRoute({
            [name]: value,
        })

    }
    render() {
        return (
            <div className="p-3">
                <div className="filter-details">
                    <div className="title">FILTERS</div>
                    <div className="details">
                        <div className="filter-titles">DEPARTURE TIME</div>
                        <ul className="dept-time">
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="dtBefore 6 am"
                                    name="dtBefore 6 am"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="dtBefore 6 am"
                                    className="form-check-label"
                                    title="Before 6 am"
                                >
                                    Before 6 am
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="dt6 am to 12 pm"
                                    name="dt6 am to 12 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="dt6 am to 12 pm"
                                    className="form-check-label"
                                    title="6 am to 12 pm"
                                >
                                    6 am to 12 pm
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="dt12 pm to 6 pm"
                                    name="dt12 pm to 6 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="dt12 pm to 6 pm"
                                    className="form-check-label"
                                    title="12 pm to 6 pm"
                                >
                                    12 pm to 6 pm
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="dtAfter 6 pm"
                                    name="dtAfter 6 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="dtAfter 6 pm"
                                    className="form-check-label"
                                    title="After 6 pm"
                                >
                                    After 6 pm
                                </label>
                            </div>
                        </ul>
                        <div className="filter-titles">BUS TYPES</div>
                        <ul className="list-chkbox">
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="bt_SEATER"
                                    name="bt_SEATER"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="bt_SEATER"
                                    className="form-check-label"
                                    title="SEATER"
                                >
                                    SEATER
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="bt_SLEEPER"
                                    name="bt_SLEEPER"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="bt_SLEEPER"
                                    className="form-check-label"
                                    title="SLEEPER"
                                >
                                    SLEEPER
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="bt_AC"
                                    name="bt_AC"
                                    onClick={this.handleFilter}
                                />

                                <label
                                    htmlFor="bt_AC"
                                    className="form-check-label"
                                    title="AC"
                                >
                                    AC
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="bt_NONAC"
                                    name="bt_NONAC"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="bt_NONAC"
                                    className="form-check-label"
                                    title="NONAC"
                                >
                                    NONAC
                                </label>
                            </div>
                        </ul>
                        <div className="filter-titles">ARRIVAL TIME</div>
                        <ul className="dept-time">
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="atBefore 6 am"
                                    name="atBefore 6 am"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="atBefore 6 am"
                                    className="form-check-label"
                                    title="Before 6 am"
                                >
                                    Before 6 am
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="at6 am to 12 pm"
                                    name="at6 am to 12 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="at6 am to 12 pm"
                                    className="form-check-label"
                                    title="6 am to 12 pm"
                                >
                                    6 am to 12 pm
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="at12 pm to 6 pm"
                                    name="at12 pm to 6 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="at12 pm to 6 pm"
                                    className="form-check-label"
                                    title="12 pm to 6 pm"
                                >
                                    12 pm to 6 pm
                                </label>
                            </div>
                            <div className="form-check">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    id="atAfter 6 pm"
                                    name="atAfter 6 pm"
                                    onClick={this.handleFilter}
                                />
                                <label
                                    htmlFor="atAfter 6 pm"
                                    className="form-check-label"
                                    title="After 6 pm"
                                >
                                    After 6 pm
                                </label>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    FilterRoute: (payload) => {
        console.log("filter");
        return {
            type: FILTER_ROUTE,
            payload,
        };
    },
};

export default connect(null, mapDispatchToProps)(FilterBusTickets);
