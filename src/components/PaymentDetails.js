import React, { Component } from "react";

import "../style/PaymentDetails.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class PaymentDetails extends Component {
    render() {
        const SearchParameters = this.props.SearchParameters;

        return (
            <>
                <header>
                    <nav className=" payment-header nav justify-content-center align-items-center p-3 gap-2">
                        <Link className="redbus-logo" to="/bus-tickets">
                            <img
                                src="https://i.postimg.cc/zXFNsGkg/redbus-white.png"
                                alt="redBus"
                                width="75px"
                            />
                        </Link>
                        <span className="src">
                            {SearchParameters.source || "Boarding city"}
                        </span>
                        <span>
                            <i className="fa-solid fa-arrow-right"></i>
                        </span>
                        <span className="dst">
                            {SearchParameters.destination || "Departure city"}
                        </span>
                    </nav>
                </header>

                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="new-info-banner d-flex justify-content-between mt-4 mb-5 px-3">
                                <div className="new-banner-chip d-flex align-items-center py-2">
                                    <img
                                        src="https://www.redbus.in/images/newPaymentTrustmarker1.svg"
                                        alt="secure"
                                    />
                                    <span className="pt-1">Secure payment</span>
                                </div>
                                <div className="new-banner-chip d-flex align-items-center">
                                    <img
                                        src="https://www.redbus.in/images/newPaymentTrustmarker2.svg"
                                        alt="refund fast"
                                    />
                                    <span className="pt-1">
                                        Lightning fast refund
                                    </span>
                                </div>
                                <div className="new-banner-chip d-flex align-items-center">
                                    <img
                                        src="https://www.redbus.in/images/newPaymentTrustmarker3.svg"
                                        alt="Reliable"
                                    />
                                    <span className="pt-1">
                                        Trusted by over 3.5Cr users
                                    </span>
                                </div>
                            </div>
                            <div className="payment-instruments">
                                <div id="pi-title-id" className="pi-title my-3">
                                    Choose Payment Method
                                </div>
                                <div className="pi-wrap">
                                    <div className="pi-subtitle my-2">
                                        UPI PAYMENT USING PHONEPE / GPAY /
                                        AMAZON PAY ETC..
                                    </div>
                                    <div className="pi mb-5">
                                        <div className="sc-qr p-2 d-flex">
                                            <div className=" p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-qr"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-qr"
                                                >
                                                    Pay through QR code
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/v2/GPayNewLogo.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/upi/phonepe.svg"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/amazonpay_new.png"
                                                />
                                            </div>
                                        </div>
                                        <div className="sc-upi p-2 d-flex">
                                            <div className="p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-upi"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-upi"
                                                >
                                                    Pay through UPI ID
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/v2/GPayNewLogo.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/upi/phonepe.svg"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/amazonpay_new.png"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="pi-subtitle my-2 mt-3">
                                        OTHER PAYMENT OPTIONS
                                    </div>
                                    <div className="pi mb-2">
                                        <div className="sc-cd p-2 d-flex">
                                            <div className="p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-cd"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-cd"
                                                >
                                                    Credit Card
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/visa.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/mastercard.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/maestro.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/rupay.png"
                                                />
                                            </div>
                                        </div>
                                        <div className="sc-dd p-2 d-flex">
                                            <div className="p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-dd"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-dd"
                                                >
                                                    Debit Card
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/visa.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/mastercard.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/maestro.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/rupay.png"
                                                />
                                            </div>
                                        </div>
                                        <div className="sc-wallets p-2 d-flex">
                                            <div className="p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-wallets"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-wallets"
                                                >
                                                    Wallets
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/mobile/v2/amazonpay_new.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/paytm.png"
                                                />
                                            </div>
                                        </div>
                                        <div className="sc-nb p-2 d-flex">
                                            <div className="p-1">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="flexRadioDefault"
                                                    id="pay-nb"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    for="pay-nb"
                                                >
                                                    Net Banking
                                                </label>
                                            </div>
                                            <div className="icon-list p-1">
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/sbi.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/hdfc.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/icici.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/axis.png"
                                                />
                                                <img
                                                    className="icon-img"
                                                    src="https://st.redbus.in/paas/images/web/v2/kotak.png"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="ticket-cart container mt-4 p-3">
                                <h6 className="bpdp-lb">BUS NAME</h6>
                                <div>
                                    <hr className="hr-bpDpSummary" />

                                    <div className="seats-selected row">
                                        <span className="seat-no col">Seat No.</span>
                                        <span className="selected-seats col text-end">
                                            <span>{"L6, L8"}</span>
                                        </span>
                                    </div>

                                    <hr className="hr-bpDpSummary" />
                                    <div className="fare-summary row">
                                        <span className="fares-lb col ">Amount</span>
                                        <span className="fare-summary-value col text-end">
                                            <span className="fare-summary-currency">INR{" "}</span>
                                            <span>{"2399"}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = (stateInStore) => {
    return {
        SearchParameters: stateInStore.users.searchParameters,
    };
};

export default connect(mapStateToProps)(PaymentDetails);
