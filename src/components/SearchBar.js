import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import DatePicker from "react-datepicker";
import Select from 'react-select';

import "react-datepicker/dist/react-datepicker.css";

import city from "../assets/images/city.png";
import date from "../assets/images/date.png";
import "../style/SearchBar.css";

import { connect } from "react-redux";
import { UPDATE_SEARCH_PARAMS } from "../redux/actions/users";

const options = [
    { value: 'bangalore', label: 'Bangalore' },
    { value: 'hyderabad', label: 'Hyderabad' },
    { value: 'mumbai', label: 'Mumbai' },
];

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: null,
            destination: null,
            date: new Date(),
            errors: {},
            isSumbitted: false,
        };
    }

    handleSourceChange = (selectOption) => {
        this.setState(
            {
                source: selectOption,
            },
            () => {
                console.log(this.state);
            }
        );
    };

    handleDestinationChange = (selectOption) => {
        this.setState(
            {
                destination: selectOption,
            },
            () => {
                console.log(this.state);
            }
        );
    };

    handleDate = (date) => {
        this.setState({
            date,
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const { source, destination } = this.state;

        if (source && destination) {
            this.props.AddSearchParameters({
                source: source.label,
                destination: destination.label,
                date: this.state.date,
            })
            this.setState({
                isSumbitted: true
            })
        }
    }
    render() {
        return (
            <>
                {this.state.isSumbitted ? (
                    <Redirect to="/bus-tickets" />
                ) : (
                    <div className="search-box container m-auto px-5">
                        <div className="source-box p-1">
                            <img
                                id="city-source"
                                src={city}
                                width="28px"
                                height="30px"
                            />

                            <Select
                                id="src"
                                className="source"
                                placeholder="From"
                                value={this.state.source}
                                onChange={this.handleSourceChange}
                                options={options}
                            />
                        </div>

                        <div className="destination-box p-1">
                            <img
                                id="city-destination"
                                src={city}
                                width="28px"
                                height="30px"
                            />

                            <Select
                                id="src"
                                className="destination"
                                placeholder="To"
                                value={this.state.destination}
                                onChange={this.handleDestinationChange}
                                options={options}
                            />
                        </div>
                        <div className="date-box py-2 px-1">
                            <img id="city-date" src={date} />

                            <DatePicker
                                dateFormat="dd/MM/yyyy"
                                className="date"
                                selected={this.state.date}
                                placeholderText="Date"
                                onChange={this.handleDate}
                            />
                        </div>
                        <button id="search_btn" className="search-button px-2" onClick={this.handleSubmit}>
                            Search Buses
                        </button>
                    </div>
                )}
            </>
        );
    }
}

const mapStateToProps = (stateInStore) => {

    return {
        currentUser: stateInStore.users.User,
        searchParameters: stateInStore.users.searchParameters,
    };
};

const mapDispatchToProps = {
    AddSearchParameters: (payload) => {
        return {
            type: UPDATE_SEARCH_PARAMS,
            payload,
        };
    },
};


export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
