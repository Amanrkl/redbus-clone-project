import React, { Component } from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import "../style/Header.css";

class Header extends Component {
    render() {
        const { isLogin } = this.props;

        return (
            <header id="rb_header" className="home-header">
                <nav className="main-header container">
                    <div className="d-flex">
                        <Link className="redbus-logo" to="/">
                            <img
                                src="https://i.postimg.cc/zXFNsGkg/redbus-white.png"
                                alt="redBus"
                                width="75px"
                            />
                        </Link>

                        <ul className="main-features navbar-nav mx-3">
                            <li className="nav-item mx-1">
                                <Link
                                    to=""
                                    title="redBus"
                                    className="nav-link"
                                    id="redBus"
                                >
                                    BUS TICKETS{" "}
                                </Link>
                            </li>
                            <li className="nav-item mx-1">
                                <Link
                                    to=""
                                    title="rYde"
                                    className="nav-link"
                                    id="rYde"
                                >
                                    rYde <sup>New</sup>{" "}
                                </Link>
                            </li>
                            <li className="nav-item mx-1">
                                <Link
                                    to=""
                                    title="redRail"
                                    className="nav-link"
                                    id="redRail"
                                >
                                    redRail <sup>New</sup>{" "}
                                </Link>
                            </li>
                        </ul>
                    </div>

                    <ul className="user-features navbar-nav">
                        <li className="nav-item mx-1">
                            <Link
                                to=""
                                title="help"
                                className="nav-link"
                                id="help"
                            >
                                Help
                            </Link>
                        </li>
                        <li className="nav-item mx-1">
                            <Link
                                to=""
                                className="nav-link"
                                id="Manage_Booking"
                            >
                                Manage Booking
                            </Link>
                        </li>

                        {!isLogin ? (
                            <li className="nav-item mx-1">
                                <div
                                    className="nav-link"
                                    id="user-login"
                                    data-bs-toggle="modal"
                                    data-bs-target="#staticBackdrop"
                                >
                                    Sign In
                                </div>
                            </li>
                        ) : (
                            <li className="nav-item dropdown">
                                <div
                                    className="nav-link"
                                >
                                    <i className="fas fa-user"></i>
                                </div>
                            </li>
                        )}
                    </ul>
                </nav>
            </header>
        );
    }
}

const mapStateToProps = (stateInStore) => {

    return {
        isLogin: stateInStore.users.User.isLogin,
    };
};

export default connect(mapStateToProps)(Header);
