import React, { Component } from "react";

import wheel from "../assets/images/steering-wheel.png";
import sleeper from "../assets/images/sleeper.png";
import femaleSleeper from "../assets/images/femaleSleeper.png";
import unavailableSleeper from "../assets/images/unavailableSleeper.png";
import selectedSleeper from "../assets/images/selectedSleeper.png";

import { connect } from "react-redux";
import { UPDATE_SELECTED_SEAT } from "../redux/actions/bus";


import "../style/Deck.css";

class Deck extends Component {
    constructor(props) {
        super(props);

        this.state = {
            deckLayer: null,
        }
    }

    handleSeatSelection = (event, seat, rowName) => {

        const deckLayer = this.state.deckLayer;
        const busId = this.props.busId;

        if (seat.seat_availability) {
            deckLayer[rowName] = deckLayer[rowName].map(rowSeat => {
                if (rowSeat.seat_no === seat.seat_no) {
                    rowSeat.seat_select = !rowSeat.seat_select;
                }
                return rowSeat
            })
            console.log("updated", deckLayer)
            this.props.updateSelectedSeat({
                id: busId,
                Selectedseat: seat,
            })
            this.setState({
                deckLayer
            })
        }
    };

    componentDidMount() {
        const deckLayer = JSON.parse(JSON.stringify(this.props.deckLayer));

        this.setState({
            deckLayer
        })
    }

    render() {
        const { deckLayer } = this.state;

        return (
            <>
                {deckLayer &&
                    <div className="deck d-flex p-2 justify-content-between gap-1">
                        <div className="wheel">
                            <img src={wheel} alt="wheel" />
                        </div>
                        <div className="layout mx-2">
                            {Object.entries(deckLayer).map(([rowName, rowDetails]) => {
                                return (
                                    <div className="busRow row w-100" key={rowName}>
                                        {rowDetails.map((seat) => {
                                            const {
                                                seat_type,
                                                seat_no,
                                                seat_availability,
                                                seat_select,
                                                seat_price,
                                            } = seat;
                                            let seatImage;
                                            if (seat_type.includes("sleeper")) {
                                                seatImage = sleeper;
                                                if (seat_type.includes("female")) {
                                                    seatImage = femaleSleeper;
                                                }
                                                if (!seat_availability) {
                                                    seatImage = unavailableSleeper;
                                                }
                                                if (seat_select) {
                                                    seatImage = selectedSleeper;
                                                }
                                            }

                                            return (
                                                <div className="col" key={seat_no}>
                                                    <img
                                                        src={seatImage}
                                                        alt="sleeper"
                                                        data-toggle="tooltip"
                                                        data-placement="right"
                                                        title={`Seat no - ${seat_no} | Fare - INR ${seat_price}`}
                                                        onClick={(event) => this.handleSeatSelection(event, seat, rowName)}
                                                    />
                                                </div>
                                            );
                                        })}
                                    </div>

                                )
                            })

                            }
                        </div>
                    </div>

                }
            </>

        );
    }
}

const mapDispatchToProps = {
    updateSelectedSeat: (payload) => {
        return {
            type: UPDATE_SELECTED_SEAT,
            payload,
        };
    },
};
export default connect(null, mapDispatchToProps)(Deck);
