import React, { Component } from 'react';

import SearchBar from '../SearchBar';
import Offers from '../Offers';
import '../../style/Home.css';
import Platforms from '../Platforms';
import Benefits from '../Benefits';

class Home extends Component {
    render() {
        return (
            <>
                <div className="search conatiner-fluid">
                    <SearchBar />
                </div>
                <div className="offers-box conatiner-fluid">
                    <Offers />
                </div>
                <div className="platform conatiner-fluid">
                    <Platforms />
                </div>
                <div className="benefits conatiner-fluid">
                    <Benefits />
                </div>
            </>
        );
    }
}

export default Home;