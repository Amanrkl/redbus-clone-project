import React, { Component } from "react";

import "../style/TicketCart.css";
import PassengerDetailsForm from "./PassengerDetailsForm";

class TicketCart extends Component {
    render() {
        const { busRoute, SeatsSelected } = this.props;
        const seatDetails = SeatsSelected.map((seat) => {
            return seat.seat_no;
        });
        const totalAmount = SeatsSelected.reduce((sum, seat) => {
            sum += seat.seat_price
            return sum
        }, 0)


        return (
            <div className="ticket-cart container w-75 mt-4 p-3">
                <h6 className="bpdp-lb">Boarding &amp; Dropping</h6>
                <div className="bpDpAddr">
                    <div className="row">
                        <span className="bpName col text-uppercase">
                            {busRoute.source_bus_stand}
                        </span>
                        <span className="bpTime col text-end">
                            {busRoute.start_time}
                        </span>
                    </div>
                    <div className="row">
                        <span className="dpName col text-uppercase">
                            {busRoute.destination_bus_stand}
                        </span>
                        <span className="dpTime col text-end">
                            {busRoute.end_time}
                        </span>
                    </div>
                </div>
                <div>
                    <hr className="hr-bpDpSummary" />

                    <div className="seats-selected row">
                        <span className="seat-no col">Seat No.</span>
                        <span className="selected-seats col text-end">
                            <span>{seatDetails.join(", ")}</span>
                        </span>
                    </div>

                    <hr className="hr-bpDpSummary" />
                    <div className="fare-summary row">
                        <span className="fares-lb col ">Amount</span>
                        <span className="fare-summary-value col text-end">
                            <span className="fare-summary-currency">INR{" "}</span>
                            <span>{totalAmount}</span>
                        </span>
                    </div>

                    <div className="continue-container row">
                        <button
                            className="continue col btn btn-danger mt-3 mx-3"
                            data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasExample"
                            aria-controls="offcanvasExample">
                            Proceed to book
                        </button>
                        <PassengerDetailsForm
                            seatDetails={seatDetails}
                            totalAmount={totalAmount} />
                    </div>
                </div>
            </div>
        );
    }
}

export default TicketCart;
