import React, { Component } from "react";

import "../style/BusTickets.css";
import BusTicket from "./BusTicket";

import { connect } from "react-redux";
import { SORT_ROUTE } from "../redux/actions/routes";
import FilterBusTickets from "./FilterBusTickets";

class BusTickets extends Component {
    filterBusRoutes = (BusRoutesList, FilterCategories) => {
        BusRoutesList = Object.entries(FilterCategories).reduce(
            (list, [filterCategoryName, filterCategoryValue]) => {
                if (filterCategoryValue) {
                    const filterOnType = filterCategoryName.slice(0, 2);

                    if (filterOnType === "dt") {
                        list = this.filterOnDepartureTime(
                            list,
                            filterCategoryName
                        );
                    } else if (filterOnType === "bt") {
                        list = this.filterOnBusType(list, filterCategoryName);
                    } else if (filterOnType === "at") {
                        list = this.filterOnArrivalTime(
                            list,
                            filterCategoryName
                        );
                    }
                }

                return list;
            },
            BusRoutesList
        );

        return BusRoutesList;
    };

    filterOnArrivalTime = (list, categoryName) => {
        return list.filter((route) => {
            const arrivalTime = Number(
                route.end_time.slice(0, route.end_time.indexOf(":"))
            );

            switch (categoryName) {
                case "atBefore 6 am": {
                    return arrivalTime < 6;
                }
                case "at6 am to 12 pm": {
                    return arrivalTime >= 6 && arrivalTime < 12;
                }
                case "at12 pm to 6 pm": {
                    return arrivalTime >= 12 && arrivalTime < 18;
                }
                case "atAfter 6 pm": {
                    return arrivalTime >= 18 && arrivalTime < 24;
                }
            }
        });
    };

    filterOnBusType = (list, categoryName) => {
        return list.filter((route) => {
            const busType = route.bus_details.type.toLowerCase();

            switch (categoryName) {
                case "bt_SEATER": {
                    return busType.includes("seater");
                }
                case "bt_SLEEPER": {
                    return busType.includes("sleeper");
                }
                case "bt_AC": {
                    return busType.includes("ac");
                }
                case "bt_NONAC": {
                    return !busType.includes("ac");
                }
            }
        });
    };

    filterOnDepartureTime = (list, categoryName) => {
        return list.filter((route) => {
            const departureTime = Number(
                route.start_time.slice(0, route.start_time.indexOf(":"))
            );

            switch (categoryName) {
                case "dtBefore 6 am": {
                    return departureTime < 6;
                }
                case "dt6 am to 12 pm": {
                    return departureTime >= 6 && departureTime < 12;
                }
                case "dt12 pm to 6 pm": {
                    return departureTime >= 12 && departureTime < 18;
                }
                case "dtAfter 6 pm": {
                    return departureTime >= 18 && departureTime < 24;
                }
            }
        });
    };

    handleSort = (event) => {
        const name = event.target.name;
        const value = !this.props.SortCategories[name];

        this.props.SortRoute({
            [name]: value,
        });
    };

    sortBusRoutes = (BusRoutesList, SortCategories) => {
        const sortByCategory = Object.entries(SortCategories).find(
            ([categoryName, categoryValue]) => {
                return categoryValue === true;
            }
        );

        if (sortByCategory) {
            BusRoutesList = this.sortByType(BusRoutesList, sortByCategory[0]);
        }

        return BusRoutesList;
    };

    sortByType = (list, categoryName) => {
        switch (categoryName) {
            case "fare": {
                return list.sort((routeA, routeB) => {
                    const priceA = Number(routeA.bus_details.prices[0]);
                    const priceB = Number(routeB.bus_details.prices[0]);

                    return priceA - priceB;
                });
            }
            case "rating": {
                return list.sort((routeA, routeB) => {
                    const rateA = routeA.bus_details.rating.rate;
                    const rateB = routeB.bus_details.rating.rate;

                    return rateA - rateB;
                });
            }
            case "seatsAvailability": {
                return list.sort((routeA, routeB) => {
                    const SeatAvailabiltyA = routeA.bus_details.available_seats;
                    const SeatAvailabiltyB = routeB.bus_details.available_seats;

                    return SeatAvailabiltyA - SeatAvailabiltyB;
                });
            }
            default: {
                return list;
            }
        }
    };

    render() {
        let BusRoutesList = this.props.BusRoutesList;
        const SearchParameters = this.props.SearchParameters;
        const FilterCategories = this.props.FilterCategories;
        const SortCategories = this.props.SortCategories;
        const { busLayout, seatSelected, SelectedBusId } = this.props;
        console.log("tickets.render",seatSelected)

        BusRoutesList = this.filterBusRoutes(BusRoutesList, FilterCategories);
        BusRoutesList = this.sortBusRoutes(BusRoutesList, SortCategories);

        return (
            <>
                <div className=" conatiner-fluid">
                    <div className="modify-sec p-3">
                        <div className="onward d-flex align-items-center gap-2">
                            <span className="src">
                                {SearchParameters.source || "Boarding city"}
                            </span>
                            <i className="fa-solid fa-arrow-right"></i>
                            <span className="dst">
                                {SearchParameters.destination ||
                                    "Departure city"}
                            </span>
                            <span className="prev">
                                <i className="fa-solid fa-angle-left"></i>
                            </span>
                            <span className="dateDispOnw">
                                <span>
                                    {SearchParameters.date
                                        ? SearchParameters.date.getDate() +
                                        " Feb"
                                        : "Date"}
                                </span>
                            </span>
                            <span className="next">
                                <i className="fa-solid fa-angle-right"></i>
                            </span>
                        </div>
                    </div>
                    <div className="search-results d-flex">
                        <div
                            className="filters"
                            disabled={seatSelected.length}
                        >
                            <FilterBusTickets />
                        </div>
                        <div className="buses d-flex flex-column">
                            <div
                                className="sort-sec row p-2"
                                disabled={seatSelected.length}
                            >
                                <div className="col-3 d-flex justify-content-between">
                                    <span className="">
                                        <span className="busFound">
                                            {BusRoutesList.length} Buses{" "}
                                        </span>
                                        found
                                    </span>
                                    <span className="">SORT BY:</span>
                                </div>
                                <div className="col-1">
                                    <a
                                        name="departure"
                                        onClick={this.handleSort}
                                    >
                                        Departure
                                    </a>
                                </div>
                                <div className="col-1">
                                    <a
                                        name="duration"
                                        onClick={this.handleSort}
                                    >
                                        Duration
                                    </a>
                                </div>
                                <div className="col-1">
                                    <a name="arrival" onClick={this.handleSort}>
                                        Arrival
                                    </a>
                                </div>
                                <div className="col-1">
                                    <a name="rating" onClick={this.handleSort}>
                                        Ratings
                                    </a>
                                </div>
                                <div className="col-2 text-center">
                                    <a name="fare" onClick={this.handleSort}>
                                        Fare
                                    </a>
                                </div>
                                <div className="col-3 text-center">
                                    <a
                                        name="seatsAvailability"
                                        onClick={this.handleSort}
                                    >
                                        Seats Available
                                    </a>
                                </div>
                            </div>
                            {BusRoutesList.length > 0 &&
                                BusRoutesList.map((busRoute) => {
                                    if (SelectedBusId === null || SelectedBusId === busRoute.id) {
                                        return (
                                            <BusTicket
                                                key={busRoute.id}
                                                busRoute={busRoute}
                                                busLayout={busLayout}
                                            />
                                        );
                                    } else {
                                        return
                                    }
                                })}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = (stateInStore) => {
    return {
        BusRoutesList: stateInStore.routes.RouteList,
        FilterCategories: stateInStore.routes.FilterCategories,
        SearchParameters: stateInStore.users.searchParameters,
        SortCategories: stateInStore.routes.SortCategories,
        busLayout: stateInStore.bus.BusLayout,
        seatSelected: stateInStore.bus.SeatsSelected,
        SelectedBusId: stateInStore.bus.SelectedBusId,
    };
};

const mapDispatchToProps = {
    SortRoute: (payload) => {
        return {
            type: SORT_ROUTE,
            payload,
        };
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(BusTickets);
