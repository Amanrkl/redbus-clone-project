import React, { Component } from "react";

import { connect } from "react-redux";
import { SIGN_IN_USER } from "../redux/actions/users";

import "../style/SignupForm.css";

class SignupForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phoneNo: "",
            isError: false,
            openOTPform: false,
            verifyOTP: false,
            OTP: "",
        };
    }

    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        let isError = false;
        let verifyOTP = false;

        if (name == "phoneNo") {
            if (value.length !== 10 || value != Number(value)) {
                isError = true;
            }
        } else if (name == "OTP") {
            if (value.length === 6 && value == Number(value)) {
                verifyOTP = true;
            }
        }

        this.setState({
            [name]: value,
            isError,
            verifyOTP,
        });
    };

    handleOTP = (event) => {
        event.preventDefault();

        if (!this.state.isError) {
            this.setState({
                openOTPform: true,
            });
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();

        if (!this.state.verifyOTP) {
            this.props.loginUser({
                isLogin: true,
                phoneNo: this.state.phoneNo,
            });
        }
    };

    render() {
        return (
            <div
                className="modal fade"
                id="staticBackdrop"
                data-bs-backdrop="static"
                data-bs-keyboard="false"
                tabIndex="-1"
                aria-labelledby="staticBackdropLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div id="signInView">
                            <div className="signin-image position-relative">
                                <div className="left-side-text">
                                    <div>Unlock the</div>
                                    <div>Smarter Way to Travel</div>
                                </div>
                                <img
                                    src="https://s3.rdbuz.com/Images/webplatform/contextualLogin/desktop-payment-offers.svg"
                                    alt=""
                                />
                            </div>

                            <div className="signin-module px-3 py-2">
                                <div className="login-container">
                                    <div className="social d-flex flex-column">
                                        <div
                                            className="redbus mb-2 d-flex w-100"
                                            id="redbusImage"
                                        >
                                            <img
                                                src="https://s3.rdbuz.com/Images/logo_r.png"
                                                alt=""
                                            />
                                            <button
                                                type="button"
                                                className="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        {this.props.currentUser.isLogin ? (
                                            <div> Sign In Successful</div>
                                        ) : (
                                            <div className="option-box d-flex flex-column w-100 align-items-center">
                                                <div className="signin-header w-75 mb-4 align-self-start">
                                                    Sign in to avail exciting
                                                    discounts and cashbacks!!
                                                </div>

                                                {this.state.openOTPform ? (
                                                    <div className="OTP-form">
                                                        <p className="message-1">
                                                            To continue, please
                                                            enter OTP sent to
                                                            verify mobile number
                                                        </p>
                                                        <h5 className="">
                                                            MOBILE NUMBER
                                                        </h5>
                                                        <div className="d-flex justify-content-between">
                                                            <p>
                                                                +91{" "}
                                                                {
                                                                    this.state
                                                                        .phoneNo
                                                                }
                                                            </p>
                                                            <p className="change">
                                                                CHANGE
                                                            </p>
                                                        </div>

                                                        <input
                                                            placeholder="ENTER OTP"
                                                            className="OTP p-2 mb-3"
                                                        />
                                                        <button
                                                            className="btn btn-danger mb-3"
                                                            onClick={
                                                                this
                                                                    .handleSubmit
                                                            }
                                                        >
                                                            VERIFY OTP
                                                        </button>
                                                        <p className="text-center">
                                                            RESEND OTP
                                                        </p>
                                                        <button className="btn btn-primary otherways">
                                                            OTHER WAYS TO SIGN
                                                            IN
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div className="d-flex flex-column w-100 align-items-center">
                                                        <div className="mobileInputContainer d-flex p-2">
                                                            <select className="countryCode">
                                                                <option value="Ind">
                                                                    +91
                                                                </option>
                                                            </select>
                                                            <input
                                                                type="text"
                                                                className="phoneNo"
                                                                name="phoneNo"
                                                                placeholder="Enter your mobile number"
                                                                onChange={
                                                                    this
                                                                        .handleInputChange
                                                                }
                                                            />
                                                        </div>
                                                        {this.state.isError && (
                                                            <div className="invalid-error">
                                                                Please enter a
                                                                valid phone
                                                                number
                                                            </div>
                                                        )}
                                                        <button
                                                            type="button"
                                                            className="otpContainer btn btn-danger py-2 mt-4 mb-3"
                                                            onClick={
                                                                this.handleOTP
                                                            }
                                                        >
                                                            GENERATE OTP (One
                                                            Time Password)
                                                        </button>

                                                        <div className="text-social text-center">
                                                            <span>
                                                                OR, Connect
                                                                using social
                                                                accounts
                                                            </span>
                                                        </div>
                                                        <div className="google my-2">
                                                            <div id="buttonDiv"></div>
                                                        </div>
                                                        <div className="facebook my-3">
                                                            <div
                                                                className="fb-login-button"
                                                                data-width=""
                                                                data-size="large"
                                                                data-button-type="continue_with"
                                                                data-layout="default"
                                                                data-auto-logout-link="false"
                                                                data-use-continue-as="false"
                                                            ></div>
                                                        </div>
                                                        <div className="paddingTerms signin-terms mb-1">
                                                            By signing up, you
                                                            agree to
                                                            <br />
                                                            our{" "}
                                                            <a href="https://www.redbus.in/info/termscondition">
                                                                Terms &amp;
                                                                Conditions
                                                            </a>{" "}
                                                            and{" "}
                                                            <a href="https://www.redbus.in/info/privacypolicy">
                                                                Privacy Policy
                                                            </a>
                                                        </div>
                                                    </div>
                                                )}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (stateInStore) => {
    return {
        currentUser: stateInStore.users.User,
    };
};

const mapDispatchToProps = {
    loginUser: (payload) => {
        return {
            type: SIGN_IN_USER,
            payload,
        };
    },
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);
